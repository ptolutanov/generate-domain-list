const fs = require('fs');

const OUTPUT_DIR = './output';
const OUTPUT_FILE_NAME = 'list.txt';

const outputFilePath = `${OUTPUT_DIR}/${OUTPUT_FILE_NAME}`;

const dataSize = fs.statSync('./data/list.csv').size;

const args = process.argv.slice(2);

if (!args.length) {
  console.error('Ошибка: не передан желаемый размер файла');
  process.exit(1);
}

const requiredSizeInMbs = +args[0];

if (isNaN(requiredSizeInMbs)) {
  console.error('Ошибка: аргумент должен быть числом');
  process.exit(1);
}

if (fs.existsSync(outputFilePath)) {
  fs.unlinkSync(outputFilePath);
}

if (!fs.existsSync(OUTPUT_DIR)) {
  fs.mkdirSync(OUTPUT_DIR);
}

const requiredSizeInBytes = requiredSizeInMbs * 1024 * 1024;
const numberOfIterations = Math.ceil(requiredSizeInBytes / dataSize);

const data  = fs.readFileSync('./data/list.csv', { encoding : 'utf8' });

for (let i = 0; i < numberOfIterations; i++) {
  fs.existsSync(outputFilePath) ? fs.appendFileSync(outputFilePath, data) : fs.writeFileSync(outputFilePath, data);
}
